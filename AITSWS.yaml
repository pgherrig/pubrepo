openapi: 3.0.1
info:
  title: AITS Web Services - API Gateway Evaluation
  description: Public APIs that can help facilitate an evaluation of API Gateway solutions.
  version: '1.0'
servers:
  - url: https://testaitsws.azure-api.net/aitsws
  - url: https://testaitsws-eastus-01.regional.azure-api.net/aitsws
paths:
  '/xfunctionalWS/data/{senderAppId}/Building/1_0/{bldgCode}':
    get:
      tags:
        - Cross-Functional API
      summary: '/xfunctionalWS/data/{senderAppId}/Building/1_0/{bldgCode} - GET'
      description: Get Building information.
      operationId: get-xfunctionalws-data-senderappid-building-1_0-bldgcode
      parameters:
        - name: senderAppId
          in: path
          description: API key owned by the sender application
          required: true
          schema:
            type: string
        - name: bldgCode
          in: path
          description: Unique identifier of a building (e.g. "1CROC")
          required: true
          schema:
            type: string
        - name: format
          in: query
          description: Desired response format. Supports either "json" (default) or "xml".
          schema:
            enum:
              - '"json"'
              - '"xml"'
            type: string
      responses:
        '200':
          description: Building
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Building'
  '/studentWS/data/{senderAppId}/Term/1_0/{termCode}/{termCampus}':
    get:
      tags:
        - Student API
      summary: '/studentWS/data/{senderAppId}/Term/1_0/{termCode}/{termCampus} - GET'
      description: Get Term data.
      operationId: get-studentws-data-senderappid-term-1_0-termcode-termcampus
      parameters:
        - name: senderAppId
          in: path
          description: API key owned by the sender application
          required: true
          schema:
            type: string
        - name: termCode
          in: path
          description: 'Valid university term code value or period to be queried (e.g. "120168", "220171", "420168", "current", "pastYear", "nextYear").'
          required: true
          schema:
            type: string
        - name: termCampus
          in: path
          description: Valid university campus owner of term code to be queried.
          required: true
          schema:
            enum:
              - '"uic"'
              - '"uis"'
              - '"uiuc"'
            type: string
        - name: format
          in: query
          description: Desired response format. Supports either "json" (default) or "xml".
          schema:
            enum:
              - '"json"'
              - '"xml"'
            type: string
      responses:
        '200':
          description: Term
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Term'
  '/studentWS/data/{senderAppId}/uicTerm/1_0/{termCode}/uic':
    get:
      tags:
        - Student API
      summary: '/studentWS/data/{senderAppId}/uicTerm/1_0/{termCode}/uic - GET'
      description: Get UIC Term Data.
      operationId: get-studentws-data-senderappid-uicTerm-1_0-termcode-termcampus
      parameters:
        - name: senderAppId
          in: path
          description: API key owned by the sender application
          required: true
          schema:
            type: string
        - name: termCode
          in: path
          description: 'Valid university term code value or period to be queried (e.g. "220171", "current", "pastYear", "nextYear").'
          required: true
          schema:
            type: string
        - name: format
          in: query
          description: Desired response format. Supports either "json" (default) or "xml".
          schema:
            enum:
              - '"json"'
              - '"xml"'
            type: string
      responses:
        '200':
          description: Term
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Term'
components:
  schemas:
    Building:
      type: object
      properties:
        object:
          type: string
          description: Business object type
        version:
          type: string
          description: Business object version
        list:
          type: array
          items:
            type: object
            properties:
              code:
                type: string
                description: Unique identifier of a building in the authoritative system
              description:
                type: string
                description: Description of a building in the authoritative system
              streetLine1:
                type: string
                description: Street address line one
              city:
                type: string
                description: Address city
              state:
                type: string
                description: Address state
              zip:
                type: string
                description: Address zip or postal code
    Term:
      type: object
      properties:
        object:
          type: string
          description: Business object type
        version:
          type: string
          description: Business object version
        list:
          type: array
          items:
            type: object
            properties:
              queryPeriod:
                type: string
                description: Replay of termCode parameter
              queryCampus:
                type: string
                description: Replay of termCampus parameter
              term:
                type: array
                items:
                  type: object
                  properties:
                    termCode:
                      type: integer
                      description: Term code identifier
                    termDescription:
                      type: string
                      description: Term code description
                    startDate:
                      type: string
                    endDate:
                      type: string
                    finaidProcYear:
                      type: object
                      properties:
                        code:
                          type: integer
                        description:
                          type: string
                    academicYear:
                      type: object
                      properties:
                        code:
                          type: integer
                        description:
                          type: string
                    housingStartDate:
                      type: string
                    housingEndDate:
                      type: string
                    termType:
                      type: object
                      properties:
                        code:
                          type: integer
                        description:
                          type: string
                    termPart:
                      type: array
                      items:
                        type: object
                        properties:
                          termCode:
                            type: integer
                          description:
                            type: string
                          startDate:
                            type: string
                          endDate:
                            type: string
                          weeks:
                            type: integer
                          censusDate:
                            type: string
                description: Array of term details
  securitySchemes:
    apiKeyHeader:
      type: apiKey
      name: Ocp-Apim-Subscription-Key
      in: header
    apiKeyQuery:
      type: apiKey
      name: subscription-key
      in: query
security:
  - apiKeyHeader: [ ]
  - apiKeyQuery: [ ]
tags:
  - name: Cross-Functional API
  - name: Security API
  - name: Student API